import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/detail_home_controller.dart';

class DetailHomeView extends GetView<DetailHomeController> {
  var name  = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$name'),
        centerTitle: true,
        backgroundColor: Colors.black54,
      ),
      body: Center(
        child: Text(
          "testing ${name}",
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
