import 'package:get/get.dart';
import 'package:getx_testing_one/app/model/productShoes.dart';
import 'package:getx_testing_one/app/model/products.dart';
import 'package:getx_testing_one/app/modules/products/providers/products_provider.dart';

import '../providers/product_shoes_provider.dart';

class ProductsController extends GetxController {
  var products = <Products>[].obs;
  var productShoes = <ProductShoes>[].obs;
  ProductsProvider productProvider=ProductsProvider();
  ProductShoesProvider productShoesProvider=ProductShoesProvider();
  RxBool isloading = false.obs;


  @override
  void onInit() {
    // getProduct();
    // fetchProduct();
    getAllPosts();
    getAllShoes();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
  Future<dynamic> getAllPosts() async {
    try {
      isloading(true);
      var data = await productProvider.fetchAllPost();
      products.value = data;
      isloading(false);
      // print(data);
      return data;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<dynamic> getAllShoes() async {
    try {
      isloading(true);
      var data = await productShoesProvider.fetchAllShoes();
      productShoes.value = data;
      isloading(false);
      return data;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

}
