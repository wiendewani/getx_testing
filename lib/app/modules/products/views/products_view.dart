import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/products_controller.dart';

class ProductsView extends GetView<ProductsController> {
  final controller = Get.put(ProductsController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text('ProductsView'),
        centerTitle: true,
      ),
      body: Center(
        child: Obx(
          () => ListView.builder(
            itemCount: controller.productShoes.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: Container(
                  width: double.infinity,
                  color: Colors.black26,
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: Center(child: Text(controller.productShoes[index].title.toString())),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
