import 'package:get/get.dart';

import '../../../model/productShoes.dart';

class ProductShoesProvider extends GetConnect {
  var header = {
    'x-rapidapi-key': 'd6fdc8c83cmsh49ed88cf8dcb32ep10cee8jsn6ad1dc495ff9',
    'x-rapidapi-host': 'nike-products.p.rapidapi.com'
  };

  Future<List<ProductShoes>> fetchAllShoes() async {
    final Response response =
        await get('https://nike-products.p.rapidapi.com/shoes/men-shoes',headers: header);
    print(response.statusCode);
    if (response.statusCode == 200) {
      final payLoad = List.from((response.body))
          .map((e) => ProductShoes.fromJson(e))
          .toList();
      return payLoad;
    } else {
      print("fecth tidak berhasil");
      return response.body;
    }
  }

}
