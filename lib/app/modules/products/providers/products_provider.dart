import 'dart:convert';
import 'package:get/get.dart';
import 'package:getx_testing_one/app/model/products.dart';

class ProductsProvider extends GetConnect {

  Future<List<Products>> fetchAllPost() async {
    final Response response =
    await get('https://jsonplaceholder.typicode.com/posts/');
    if (response.statusCode == 200) {
      final payLoad = List.from((response.body))
          .map((e) => Products.fromJson(e))
          .toList();
      return payLoad;
    } else {
      return response.body;
    }
  }

}
