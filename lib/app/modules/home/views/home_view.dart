import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_testing_one/app/modules/detail_home/views/detail_home_view.dart';
import 'package:getx_testing_one/app/modules/products/views/products_view.dart';
import '../../detail_home/controllers/detail_home_controller.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final item = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeView'),
        centerTitle: true,
        backgroundColor: Colors.black54,
      ),
      body: Column(
        children: [
          Container(
            height: 35,
            color: Colors.black12,
            child: Center(
              child: Obx(
                () => Text("Total Data : ${controller.listLaptop.length}"),
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.all(8),
              child: Obx(
                () => ListView.builder(
                  itemCount: controller.listLaptop.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Get.put(DetailHomeController());
                        Get.to(() => DetailHomeView(),
                            arguments: [controller.listLaptop[index]]);
                      },
                      child: Container(
                        width: double.infinity,
                        color: Colors.black26,
                        margin: const EdgeInsets.symmetric(vertical: 5),
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child:
                            Center(child: Text(controller.listLaptop[index])),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: RaisedButton(
              onPressed: () {
                Get.to(()=> ProductsView());
              },
              child: const Text('Klik', style: TextStyle(fontSize: 20)),
              color: Colors.black87,
              textColor: Colors.white,
              elevation: 5,
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.black87,
          child: const Icon(Icons.add),
          onPressed: () {
            Get.dialog(AlertDialog(
              title: Text("Add Item"),
              content: TextField(controller: item.itemController),
              actions: [
                ElevatedButton(
                    onPressed: () {
                      controller.listLaptop.add(item.itemController.text);
                      item.itemController.clear();
                      Get.back();
                    },
                    child: Text('Add')),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cencel')),
              ],
            ));
          }),
    );
  }
}
