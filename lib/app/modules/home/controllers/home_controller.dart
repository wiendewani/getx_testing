import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {


  final listLaptop = ["XP", "ASUS", "ACER", "MSI"].obs;
  final count = 0.obs;
  final itemController = TextEditingController();

  @override
  void onInit() {
    update();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
