class ProductShoes {
  String? img;
  String? url;
  String? title;
  String? price;
  String? source;

  ProductShoes({this.img, this.url, this.title, this.price, this.source});

  ProductShoes.fromJson(Map<String, dynamic> json) {
    img = json['img'];
    url = json['url'];
    title = json['title'];
    price = json['price'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['img'] = this.img;
    data['url'] = this.url;
    data['title'] = this.title;
    data['price'] = this.price;
    data['source'] = this.source;
    return data;
  }
}